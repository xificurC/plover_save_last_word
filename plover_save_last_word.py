import os
from plover.formatting import RetroFormatter

SAVE_FILE = os.getenv('PLOVER_SAVE_LAST_WORD_FILE',
                      os.getenv('HOME') + '/plover_saved_words')


def save_last_word(engine, argument):
    last_word = next(RetroFormatter(engine.translator_state.translations[:-1]).iter_last_words())
    with open(SAVE_FILE, 'a+') as f:
        print(last_word, file=f)
